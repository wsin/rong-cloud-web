import Vue from 'vue'
import App from './App.vue'
import router from './router'
import RCLiveRoomLib from "@rongcloud/rcliveroomlib";

Vue.config.productionTip = false
Vue.prototype.$RCLiveRoomLib = RCLiveRoomLib
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')