import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import Live from '@/components/live/Live.vue'
import Room from '@/components/room/Room.vue'

const routes = [{
    path: "/live",
    name: "Live",
    component: Live,
    meta: {
      title: "开播"
    },
  },
  {
    path: "/room",
    name: "Room",
    component: Room,
    meta: {
      title: "播放"
    },
  }
]

const router = new VueRouter({
  routes,
  mode: 'hash'
})

export default router